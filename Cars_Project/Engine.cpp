#include "Engine.h"

Engine::Engine(int horsepower, float capacity, string fueltype)
{
	this->m_hp = horsepower;
	this->m_cap = capacity;
	this->m_fuel = fueltype;
}

Engine::Engine()
{
	this->m_cap = 0;
	this->m_hp = 0;
	this->m_fuel = "";
}
ostream& operator<<(ostream& os, const Engine& eng)
{
	os << eng.m_hp << "," << eng.m_cap << "," << eng.m_fuel << ",";
	return os;
}
