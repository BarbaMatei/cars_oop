#pragma once
#include <string>
#include "Cars.h"
using namespace std;
class Engine :public Cars
{
public:
	Engine(int horsepower, float capacity, string fueltype);
	Engine();
	int getHP() const{ return this->m_hp; };
	float getcap()const { return this->m_cap; };
	string getFuel()const { return this->m_fuel;};
	friend ostream& operator<<(ostream& os, const Engine& eng);
private:
	int m_hp, m_year;
	float m_cap;
	string m_fuel;
};

