#pragma once
#include "Cars.h"
class LuxuryCars:public Cars
{
public:
	LuxuryCars(int id, string model, int year, int ComfortLevel);
	void display(ostream& os)const override;
private:
	int m_ComfortLevel;
};

