#pragma once
#include "Cars.h"
class SportCars:public Cars
{
public:
	SportCars(int id, string model, int year, int quartermile_time);
	void display(ostream& os)const override;
private:
	int m_quartermile_time;

};

