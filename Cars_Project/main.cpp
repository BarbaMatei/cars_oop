#include "UserInterface.h"
#include "CarsController.h"
#include "CarsRepository.h"
int main() {
	CarsRepository repo;
	CarsController controller(repo);
	UserInterface ui(controller);
	ui.show();
	return 0;
}